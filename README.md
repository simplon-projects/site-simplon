# Site de présentation simplon 
## Binôme : Audrey et Myriam

    Ce site présente des informations sur la formation web développeur de simplon Grenoble
   
## Consignes :

    Créer un mini site pour le centre de formation Simplon de la ville de la promo.
    Le site devra présenter la formation Simplon ainsi que donner les informations utiles sur la fabrique de Simplon.
    Livrables :
    Un dépot Gitlab contenant votre code source (HTML, SASS, CSS, Images, ...), ainsi que le schéma de votre maquette graphique. Ce schéma doit être au format image (JPG, PNG) et peut être fait soit sur un outil numérique ou alors juste en mode papier/crayon (prendre une photo)

## Contexte du projet

* Vous devez respecter la __charte graphique__ du groupe Simplon (couleur, police d'écriture, etc ...). Vous devez aussi intégrer le logo du Simplon.
* Le site devra être réalisé en __HTML et CSS__.
* Aucune __maquette graphique__ n'a été définie pour le site, vous avez carte blanche.
* Le site doit être __responsive__.
* Vous devez utiliser __SASS__ pour la création de votre CSS
* Vous devez utiliser une __convention de nommage__
* Le site doit contenir un __formulaire de contact__, non fonctionnel mais les __champs__ sont __validés__ :
    * *Nom*
    * *Prenom*
    * *Genre* (Mr / Mme / Autre)
    * *Email*
    * *Message* (max 300 caractères)
* Les __images__ doivent être __optimisées__ pour le web (taille adaptée / poids etc)
* Les champs dédiés au __référencement__ doivent être complétés (__images et meta__)
* Vous __pouvez utiliser un framework CSS__ : Bootstrap ou autre
* Vous __pouvez utiliser toutes les librairies CSS ou JS__ que vous voulez

## Modalités pédagogiques

    Le travail est à faire en groupe de 2.
    A rendre pour le 03 juillet 2020 au soir.